package crawler;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/* Simple Web Crawler
    Meant for small list of links.
    Add concurrency to scale to a large list of links, but
    seemed out of scope for the requirements
 */
public class Crawler {
    private Set<String> visited = new HashSet<>();
    private List<String> success = new LinkedList<>();
    private List<String> failed = new LinkedList<>();
    private List<String> duplicate = new LinkedList<>();

    //Total Requests Made
    public Set<String> getVisited() {
        return visited;
    }

    //Successful Requests
    public List<String> getSuccess() {
        return success;
    }

    //Failed Requests
    public List<String> getFailed() {
        return failed;
    }

    //Duplicate Requests
    public List<String> getDuplicate() {
        return duplicate;
    }

    //JSON Property containing array of links
    public static String LINKS_PROPERTY = "links";

    //Get content from a given URL
    public static String getContent(String Url) {
        try {
            return Jsoup.connect(Url).ignoreContentType(true).followRedirects(true).method(Connection.Method.GET).execute().body();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return "";
        }
    }

    //Parse Array of Strings from a JSON object given a Property.
    @SuppressWarnings("unchecked")
    public static List<String> parseLinks(String json, String property) {
        List<String> links = new LinkedList<>();
        JSONParser parser = new JSONParser();
        try {
            JSONObject obj = (JSONObject) parser.parse(json);
            links = (List<String>) obj.get(property);
        } catch(ParseException e){
            System.out.println(e.toString());
        }
        return links;
    }

    //Output Stats for all previously run crawls
    public void printStats(){
        System.out.println("Requests:   " + getVisited().size());
        System.out.println("Success:    " + getSuccess().size());
        System.out.println("Failed:     " + getFailed().size());
        System.out.println("Duplicates: " + getDuplicate().size());
    }

    //crawl a json string using the default property to get array of strings
    public void crawl(String json){
        crawl(json,LINKS_PROPERTY);
    }

    //crawl a json string using a custom property to get array of strings
    public void crawl(String json, String property) {
        List<String> links = parseLinks(json, property);
        crawl(links);
    }

    //crawl a list of strings
    public void crawl(List<String> links) {
        if(null == links){
            return;
        }
        getLinks(links);
    }

    //crawl list of links by starting with initial links and
    //recursively looping over links contained in responses
    //log results to properties for later output
    private void getLinks(List<String> links) {
        List<String> urls = new LinkedList<>();
        for (String s : links) {
            if (!getVisited().add(s)) {
                getDuplicate().add(s);
                continue;
            }
            try {
                Connection.Response response = Jsoup.connect(s).ignoreHttpErrors(true).execute();
                if (response.statusCode() >= 400) {
                    getFailed().add(s);
                    continue;
                }
                Document doc = response.parse();
                Elements hrefs = doc.select("a[href]");
                for (Element href : hrefs) {
                    urls.add(href.absUrl("href"));
                }
                getSuccess().add(s);

                if (urls.size() == 0)
                    continue;

                getLinks(urls);
            } catch (Exception e) {
                getFailed().add(s);
                System.out.println(e.getMessage());
            }
        }
    }
}
