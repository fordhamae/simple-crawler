/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package crawler;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

import static java.util.Arrays.*;
import static java.util.Collections.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class AppTest {

    @Test
    public void testApp(){
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        App.main(new String[]{});
        String expectedOutput = "Requests:   192\nSuccess:    188\nFailed:     4\nDuplicates: 13201\n";
        assertEquals("Failed Output", expectedOutput, outContent.toString());
    }

    @Test
    public void testCrawlerWithJsonUrl() throws Exception {
        Crawler crawler = new Crawler();
        String json = Crawler.getContent("https://raw.githubusercontent.com/OnAssignment/compass-interview/master/developer/data.json");
        crawler.crawl(json);
        assertEquals("Failed should be 4.", 4, crawler.getFailed().size());
        assertEquals("Requests should be 192.", 192, crawler.getVisited().size());
        assertEquals("Success should be 188.", 188, crawler.getSuccess().size());
        assertEquals("Duplicates should be 13201.", 13201, crawler.getDuplicate().size());
    }

    @Test
    public void testCrawlerWithListOfUrls() {
        Crawler crawler = new Crawler();
        crawler.crawl(new LinkedList<>(asList("https://httpbin.org/links/1", "https://httpbin.org/links/2")));
        assertEquals("Failed should be 0.", 0, crawler.getFailed().size());
        assertEquals("Requests should be 4.", 4, crawler.getVisited().size());
        assertEquals("Success should be 4.", 4, crawler.getSuccess().size());
        assertEquals("Duplicates should be 1.", 1, crawler.getDuplicate().size());
    }

    @Test
    public void testCrawlerDuplicates(){
        Crawler crawler = new Crawler();
        List<String> expected = new LinkedList<>(singletonList("https://httpbin.org/links/1"));
        crawler.crawl(new LinkedList<>(asList("https://httpbin.org/links/1", "https://httpbin.org/links/1")));
        assertEquals("Duplicates should be 1.", 1, crawler.getDuplicate().size());
        assertEquals("Success should be 1.", 1, crawler.getSuccess().size());
        assertEquals("Requests should be 1.", 1, crawler.getVisited().size());
        assertEquals("Failed should be 0.", 0, crawler.getFailed().size());
        assertThat("Duplicate urls match", crawler.getDuplicate(), is(expected));
    }

    @Test
    public void testCrawlerRequests(){
        Crawler crawler = new Crawler();
        HashSet<String> expected = new HashSet<>(asList("https://httpbin.org/links/1", "https://httpbin.org/status/200"));
        crawler.crawl(new LinkedList<>(asList("https://httpbin.org/links/1", "https://httpbin.org/status/200")));
        assertEquals("Duplicates should be 0.", 0, crawler.getDuplicate().size());
        assertEquals("Success should be 2.", 2, crawler.getSuccess().size());
        assertEquals("Requests should be 2.", 2, crawler.getVisited().size());
        assertEquals("Failed should be 0.", 0, crawler.getFailed().size());
        assertThat("Visited urls match", crawler.getVisited(), is(expected));
    }

    @Test
    public void testCrawlerSuccesses(){
        Crawler crawler = new Crawler();
        List<String> expected = new LinkedList<>(asList("https://httpbin.org/links/1", "https://httpbin.org/status/200"));
        crawler.crawl(new LinkedList<>(asList("https://httpbin.org/links/1", "https://httpbin.org/status/200")));
        assertEquals("Duplicates should be 0.", 0, crawler.getDuplicate().size());
        assertEquals("Success should be 2.", 2, crawler.getSuccess().size());
        assertEquals("Requests should be 2.", 2, crawler.getVisited().size());
        assertEquals("Failed should be 0.", 0, crawler.getFailed().size());
        assertThat("Successful urls match", crawler.getSuccess(), is(expected));
    }

    @Test
    public void testCrawlerFails(){
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        String expectedOutput = "Unhandled content type. Must be text/*, application/xml, or application/xhtml+xml\n";
        Crawler crawler = new Crawler();
        List<String> expected = new LinkedList<>(asList("https://httpbin.org/status/500", "https://httpbin.org/status/301"));
        crawler.crawl(new LinkedList<>(asList("https://httpbin.org/status/500", "https://httpbin.org/status/301")));
        assertEquals("Duplicates should be 0.", 0, crawler.getDuplicate().size());
        assertEquals("Success should be 0.", 0, crawler.getSuccess().size());
        assertEquals("Requests should be 2.", 2, crawler.getVisited().size());
        assertEquals("Failed should be 2.", 2, crawler.getFailed().size());
        assertThat("Failed urls match", crawler.getFailed(), is(expected));
        assertEquals("301 does not return expected content", expectedOutput, outContent.toString());
    }

    @Test
    public void testCrawlerWrongLinksPropertyInJson(){
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        Crawler crawler = new Crawler();
        crawler.crawl("{\"urls\": [1,2,3]}");
        String expectedOutput = "";
        assertEquals("Wrong Links Property in JSON", expectedOutput, outContent.toString());
    }

    @Test
    public void testCrawlerCustomLinksPropertyInJson(){
        Crawler crawler = new Crawler();
        crawler.crawl("{\"urls\": [\"https://httpbin.org/links/1\", \"https://httpbin.org/status/200\"]}","urls");
        assertEquals("Duplicates should be 0.", 0, crawler.getDuplicate().size());
        assertEquals("Success should be 2.", 2, crawler.getSuccess().size());
        assertEquals("Requests should be 2.", 2, crawler.getVisited().size());
        assertEquals("Failed should be 0.", 0, crawler.getFailed().size());
    }

    @Test
    public void testCrawlerLinksPropertyConstantInJson(){
        Crawler crawler = new Crawler();
        crawler.crawl("{\"links\": [\"https://httpbin.org/links/1\", \"https://httpbin.org/status/200\"]}",Crawler.LINKS_PROPERTY);
        assertEquals("Duplicates should be 0.", 0, crawler.getDuplicate().size());
        assertEquals("Success should be 2.", 2, crawler.getSuccess().size());
        assertEquals("Requests should be 2.", 2, crawler.getVisited().size());
        assertEquals("Failed should be 0.", 0, crawler.getFailed().size());
    }

    @Test
    public void testPrintStats(){
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        Crawler crawler = new Crawler();
        crawler.printStats();
        String expectedOutput = "Requests:   0\nSuccess:    0\nFailed:     0\nDuplicates: 0\n";
        assertEquals("Failed Output", expectedOutput, outContent.toString());
    }

    @Test
    public void testParseJsonLinks() {
        Crawler crawler = new Crawler();
        List<String> expected = asList("https://httpbin.org/links/1","https://httpbin.org/links/10");
        String json  = "{\"links\": [\"https://httpbin.org/links/1\",\"https://httpbin.org/links/10\"]}";
        List<String> actual = crawler.parseLinks(json, "links");
        assertThat("json object cast",actual,is(expected));
        assertEquals( "json parse links",2, actual.size());
    }

    @Test
    public void testParseJsonLinksCastWithInts() {
        Crawler crawler = new Crawler();
        List<String> links = crawler.parseLinks("{\"links\": [1,2,3,4,5,6]}", "links");
        assertEquals( "ints for links",6, links.size());
    }

    @Test
    public void testParseJsonWithBadJsonFormat() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        Crawler crawler = new Crawler();
        List<String> links = crawler.parseLinks("{\"links\": [1,}","links");
        assertEquals( "invalid json",0, links.size());
        String expectedOutput = "Unexpected token RIGHT BRACE(}) at position 13.\n";
        assertEquals("Failed Output", expectedOutput, outContent.toString());
    }

    @Test
    public void testCrawlerWithBadLinksList() {
        Crawler crawler = new Crawler();
        crawler.crawl(new LinkedList<>(asList("1","2","2","3")));
        assertEquals("Failed should be 3.", 3, crawler.getFailed().size());
        assertEquals("Requests should be 3.", 3, crawler.getVisited().size());
        assertEquals("Success should be 0.", 0, crawler.getSuccess().size());
        assertEquals("Duplicates should be 1.", 1, crawler.getDuplicate().size());
    }

    @Test
    public void testAppBadJsonUrl(){
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        App app = new App();
        app.crawl("https://www.google.com/");
        String expectedOutput = "Unexpected character (<) at position 0.\nRequests:   0\nSuccess:    0\nFailed:     0\nDuplicates: 0\n";
        assertEquals("Bad JSON URL", expectedOutput, outContent.toString());
    }

    @Test
    public void testGetJson404(){
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        String json = Crawler.getContent("https://httpbin.org/status/404");
        String expectedOutput = "HTTP error fetching URL\n";
        assertEquals("404 JSON URL", expectedOutput, outContent.toString());
    }

    @Test
    public void testGetJson500(){
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        String json = Crawler.getContent("https://httpbin.org/status/500");
        String expectedOutput = "HTTP error fetching URL\n";
        assertEquals("500 JSON URL", expectedOutput, outContent.toString());
    }
}
